<?php

$siteName = "php-mvc-implementation";
$appName = "MVC for PHP";
$baseURI = "http://" . $_SERVER['SERVER_NAME'] . "/" . $siteName . "/";

//  Supress PHP all warnings.
//error_reporting(0);

//  Get formatted and simple Request URI
$path = explode('/', trim($_SERVER['SCRIPT_NAME'], '/'));

$requestURI  = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

foreach ($path as $key => $val) {
    if ($val == $requestURI[$key]) {
        unset($requestURI[$key]);
    } else {
        break;
    }
}

$requestURI = implode('/', $requestURI);
$requestURI = str_replace("?" . $_SERVER["QUERY_STRING"], "", $requestURI);

$sites = array(
    "login" => "",
    "someSite" => "index.php"
);

$sitesNumeric = array();
$similarSites = array();

foreach($sites as $x=>$x_value){
    array_push($sitesNumeric, $x_value);
}

for ($i = 0; $i < sizeof($sitesNumeric); $i++) {
    similar_text($requestURI, $sitesNumeric[$i], $percent);
    if ($percent >= 50) {
        array_push($similarSites, $sitesNumeric[$i]);
    }
}