<?php

/**
 * hypothetical class used for this example
 *
*/
class HypoClass {
	private $data

	public function __construct() {}

	public function setData(type $data) {
		$this->data = $data;
	}

	public function getData() {
		return $this->data;
	}
}