<?php


/*  
    Include your Semantic │ Concrete Classes. In this case, i'll be using a semantic class
    that manages my concrete class instances.
*/
require_once ("../bean/dao/HypoClassDAO.php");

/*
    Start │ Resume your HTTP Sessions, in case you're implementing them.
*/
session_start();


/*
    Validate access to your views according to your conditions.
    In this case, I'll be validating HTTP Session Attributes, but
    you could add your own validations such as Business Rules.
*/
if (isset($_SESSION["SESSION-ATTRIBUTE"])) {    //  Session Attribute is set, so a session is being resumed, not started.
    header("Location: " . $baseURI . $sites["blog"]);
    exit;
} else {
    if (isset($_POST["PARAMETER"])) { //  A HTTP Session is being started and a HTTP POST parameters was received.

        if (!is_null($_GET["PARAMETER"])) {  //  Said HTTP POST parameter is not null.
            $data = $_GET["PARAMETER"];

            $validation = HypoClassDAO::checkSomething($data);

            if ($validation) {  //  Validation passed the test.
                $httpSessionObject = HypoClassDAO::getSpecificData($data);
                
                /*
                    Store your desired data into a HTTP Session Attribute.
                    In this case, I storaged a HypoClass Class Object.
                */
                $_SESSION["SESSION-ATTRIBUTE"] = $httpSessionObject;

                /*
                    Request a view or perform a redirect.
                */
                require_once ("../view/MyDynamicView.php");
            } else {
                require_once ("../view/Login.php");
            }
        }    
    } else {
        require_once ("../view/Login.php");
    }
}