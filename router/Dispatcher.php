<?php

require_once ("../Config.php");

switch ($requestURI) {
	/*
		Get a view via controller for dinamic content
	*/
    case $sites["login"]:
        require_once("MyRouter.php");
        break;
    /*
    	Get a view directly for static content
    */
    case $sites["someSite"]:
    	require_once ("../view/MyView.php");
    	break;
	/*
		Tell the user that you could not find the requested site
	*/
    default:
        require_once ("Missing.php");
}