<?php 
    class Connection {
        
        public static $host;
        public static $user;
        public static $password;
        public static $database;
        public static $connection = NULL;

        public static function getConnection() {
            
            Connection::getProperties();

            Connection::$connection = new mysqli(Connection::$host, Connection::$user, Connection::$password, Connection::$database);

            if (Connection::$connection->connect_error) {
                die("[ERROR] Could not connect to database │ " . Connection::$connection->connect_error);
            }
        }

        public static function doQuery($query) {
            Connection::getConnection();
            
            $result = Connection::$connection->query($query);
            if ($result === TRUE || is_a($result, 'mysqli_result')) {
                //echo "New record created successfully";
            } else {
                echo "Error: " . $query . "<br>" . Connection::$connection->error;
            }            
            
            Connection::endConnection();            
            return $result;
        }
        
        public static function getProperties() {
            $json = file_get_contents("../database.json");
            $jsonIterator = new RecursiveIteratorIterator(
                new RecursiveArrayIterator(json_decode($json, TRUE)), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($jsonIterator as $key => $val) {
                if ($key == "dev") {
                    Connection::$host = $val["host"];
                    Connection::$user = $val["user"];
                    Connection::$password = $val["password"];
                    Connection::$database = $val["database"];
                }
            }
        }
        
        public static function endConnection() {
            mysqli_close(Connection::$connection);
        }
    }